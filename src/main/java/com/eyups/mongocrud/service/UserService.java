package com.eyups.mongocrud.service;


import com.eyups.mongocrud.model.User;
import com.eyups.mongocrud.repository.UserRepository;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService {

    private final UserRepository userRepository;

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User getUser(String id) throws NotFoundException {
        Optional<User> byId = userRepository.findById(id);
        return byId.orElseThrow(()->new NotFoundException("user id not found, snap out of it  "));
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }
}
