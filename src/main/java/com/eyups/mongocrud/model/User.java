package com.eyups.mongocrud.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.List;

@Data
@Document
public class User {
    @Id
    @JsonIgnore
    private String id;
    private String username;
    private String password;
    private List<String> skills;
    private HashMap<String,String> others;
}
