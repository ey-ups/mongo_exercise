package com.eyups.mongocrud.repository;

import com.eyups.mongocrud.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User,String> {
}
