package com.eyups.mongocrud.controller;

import com.eyups.mongocrud.model.User;
import com.eyups.mongocrud.service.UserService;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    @PostConstruct
    public void init() {
        HashMap<String, String> other = new HashMap<>();
        User user = new User();
        user.setUsername("ey-up");
        user.setId("1");
        other.put("selam","naber");
        other.put("hi","whatsup");
        user.setOthers(other);
        user.setPassword("123");
        user.setSkills(List.of("basketball", "software", "flight"));
        userService.createUser(user);
    }

    @GetMapping
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok(userService.getUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable String id) throws NotFoundException {
        return ResponseEntity.ok(userService.getUser(id));
    }

    @PostMapping()
    public ResponseEntity<User> createUser(@RequestBody User user) {
        return ResponseEntity.ok(userService.createUser(user));
    }
}
